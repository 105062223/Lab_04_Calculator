function display(value) {
    if(value==="C"){
      document.getElementById("screen").value = "";
    }
    else{
      document.getElementById("screen").value += value;
    }
}

function result() {
    var correct = 1;

    try{
        ans = eval(document.getElementById("screen").value);
    } catch(e){
        if(e instanceof SyntaxError){
            document.getElementById("screen").value = "error";
            correct = 0;
        }
    }

    if(correct===1){
        document.getElementById("screen").value = ans;
    }
}